﻿//define obj
var resdata ={};
//获取URL
function getWebsiteAndShowBtn(tabId, changeInfo, tab) {
	//获取上下文URL
	resdata.url=tab.url;
	//显示按钮
	chrome.pageAction.show(tabId);
};
//添加事件监听
chrome.tabs.onUpdated.addListener(getWebsiteAndShowBtn);

//获取切换tab的事件
function checkTabChange(activeInfo){
	//通过tabid获取tab
	chrome.tabs.get(activeInfo.tabId, function(tab){
	//获取上下文URL
	resdata.url=tab.url;
	})
}
//获取切换tab的事件
chrome.tabs.onActivated.addListener(checkTabChange);